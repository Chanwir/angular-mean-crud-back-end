require("dotenv").config();
const express = require("express");
const path = require("path");
const cors = require("cors");
const mongoose = require("mongoose");

const db = "mongodb://localhost:27017/db";
mongoose.Promise = global.Promise;
mongoose
  .connect(db, {
    useUnifiedTopology: true,
  })
  .then(
    () => {
      console.log("Database successfully connected");
    },
    (err) => {
      console.log("Database error : " + err);
    }
  );

const bookRoute = require("./routes/book.routes");
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

// Static directory
app.use(express.static(path.join(__dirname, "dist/")));
// Base route
app.get("/", (rea, res) => {
  res.sendFile(path.join(__dirname, "dist/index.html"));
});

// API Root
app.use("/api", bookRoute);

//Port
const port = process.env.PORT || 8080;

app.listen(port, () => {
  console.log("Listening on port " + port);
});

//404 Handler
app.use((req, res, next) => {
  res.status(404).send("Not Found");
});

// erroe handler

app.use((err, req, res, next) => {
  console.log(err.message);
  if (!err.statusCode) err.statusCode = 500;
  res.status(err.statusCode).send(err.message);
});
